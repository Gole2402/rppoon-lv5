﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("dat.txt");
            User user1 = new User("Dino",1);
            User user2 = new User("Petar",5);
            User user3 = User.GenerateUser("David");
            User user4 = User.GenerateUser("Josip");
            User user5 = new User("Marko",3);

            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(user3);
            ProtectionProxyDataset proxy4 = new ProtectionProxyDataset(user4);
            ProtectionProxyDataset proxy5 = new ProtectionProxyDataset(user5);

            VirtualProxyDataset proxy6 = new VirtualProxyDataset("dat.txt");
            DataConsolePrinter printer = new DataConsolePrinter();

            printer.Print(proxy1);
            Console.WriteLine();

            printer.Print(proxy2);
            Console.WriteLine();

            printer.Print(proxy3);
            Console.WriteLine();

            printer.Print(proxy4);
            Console.WriteLine();

            printer.Print(proxy5);
            Console.WriteLine();

            Console.WriteLine("Virtual: \n");
            printer.Print(proxy6);
        }
    }
}
