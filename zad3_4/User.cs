﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3_4
{
    class User
    {
        private static int id = 0;
        public string Name { get; private set; }
        public int ID { get; private set; }
        public User(string name, int ID)
        {
            this.Name = name;
            this.ID = ID;
        }
        public static User GenerateUser(string name)
        {
            id++;
            return new User(name, id);
        }
    }
}
