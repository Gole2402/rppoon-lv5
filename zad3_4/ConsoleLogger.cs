﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad3_4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;

        private ConsoleLogger() { }

        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }

        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
