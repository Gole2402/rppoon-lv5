﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad1_2
{
    class ShippingService
    {
        private double cost;

        public ShippingService(double cost) { this.cost = cost; }

        public double Price(double weight)
        {
            return cost * weight;
        }

        public override string ToString()
        {
            return "Ukupna cijena dostave je:";
        }
    }
}
