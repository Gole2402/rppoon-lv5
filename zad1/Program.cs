﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zad1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box box = new Box("shoplist");
            Product product1 = new Product("PlayStation 4 Pro", 2500, 5);
            Product product2 = new Product("Samsung Galaxy S20", 8500, 1.8);
            box.Add(product1);
            box.Add(product2);


            ShippingService price = new ShippingService(5);
            double weight = box.Weight;
            

            Console.WriteLine(price.ToString() + price.Price(weight) + " HRK");
        }
    }
}
